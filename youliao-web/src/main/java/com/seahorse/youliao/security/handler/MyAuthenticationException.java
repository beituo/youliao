package com.seahorse.youliao.security.handler;

import org.springframework.security.core.AuthenticationException;

/**
 * @ProjectName: youliao
 * @Package: com.seahorse.youliao.security
 * @ClassName: MyAuthenticationException
 * @Description: 自定义认证异常
 * @author:songqiang
 * @Date:2020-11-25 16:23
 **/
public class MyAuthenticationException extends AuthenticationException {


    /**
     * Constructs a new instance of AuthenticationException.
     * All fields are set to null.
     */
    public MyAuthenticationException(String msg) {
        super(msg);
    }
}
