package com.seahorse.youliao.security.handler;

import com.zengtengpeng.operation.RedissonObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @description: 退出成功处理器
 * @author: Mr.Song
 * @create: 2020-11-28 16:08
 **/
@Component
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private RedissonObject redissonObject;

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {


        // 退出成功 清除token
        String token = httpServletRequest.getHeader("token");
        if(StringUtils.isNotBlank(token)){
            redissonObject.delete(token);
        }
    }
}
