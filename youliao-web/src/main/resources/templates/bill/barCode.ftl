﻿﻿<html xmlns:th="http://www.thymeleaf.org">
<head>
    <#--https://lindell.me/JsBarcode/-->
    <meta charset="UTF-8">
    <title>barQrCode</title>
    <script src="https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/jsbarcode/3.11.0/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>
    <style  type="text/css">

        body { text-align: center;}

    </style>

</head>
<body style="height:100%; margin:0; overflow:hidden;">
<div class="page">
    <div class="container">
        <!--顶部-->
        <div class="top">
            <span class="card-name">请向收银员出示优惠券码</span>
        </div>
        <!--条码-->
        <div class="barcode-container">
            <div>条码展示</div>
            <img id="barcode"/>
        </div>
        <!--二维码-->
        <div class="qrcode-container">
            <div>二维码展示</div>
            <div id="qrcode"></div>
        </div>
    </div>
</div>
<script type="text/javascript" style="width:100%;height:100%;">
    var coupon_code = "20191227123456789";//这里是需要生成图片的字符串
    $(function () {
    $("#barcode").JsBarcode(coupon_code, {
    width: 2.5,// 设置条之间的宽度
    height: 80,// 高度
    font: "Songti SC", // 设置文本的字体
    fontSize: 18, // 设置文本的大小
    lineColor: "black", // 设置条和文本的颜色。
    });
    jQuery('#qrcode').qrcode({width: 150, height: 150, text: coupon_code})
    })
</script>
</body>
</html>

