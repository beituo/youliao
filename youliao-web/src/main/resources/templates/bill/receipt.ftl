﻿﻿<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>receipt</title>
    <script type="text/javascript" src="/api/js/X-doc.js"></script>
</head>
<body style="height:100%; margin:0; overflow:hidden;">
<script id="myxdoc" type="text/xdoc" _format="pdf" style="width:100%;height:100%;">
    <xdoc version="A.3.0">
    <paper margin="0" width="300" height="380"/>
    <body padding="10" fillImg="#@f40">

        <para align="center" lineSpacing="15">
            <text fontName="行楷" fontSize="25" fontStyle="shadow">销售小票</text>
        </para>
        <para align="center" lineSpacing="5">
            <text fontColor="#ff0000" fontName="标宋" fontSize="12">2019-12-29 14:45</text>
        </para>
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="8">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</text>
        </para>
        <para >
            <rect color="" name="x6870" width="150">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="150">
                    <text fontName="标宋" fontSize="12">1:康师傅老坛酸菜方便面*3</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="100">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
                    <text  fontName="仿宋" fontSize="12">$12.00</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="150">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="150">
                    <text fontName="标宋" fontSize="12">2:蒙古牛肉 * 3袋</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="100">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
                    <text  fontName="仿宋" fontSize="12"> $220.00</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="150">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="150">
                    <text fontName="标宋" fontSize="12">3:鲤鱼一条(5斤)</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="100">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
                    <text  fontName="仿宋" fontSize="12">$33.50</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="150">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="150">
                    <text fontName="标宋" fontSize="12">4:花生油一桶</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="100">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
                    <text  fontName="仿宋" fontSize="12">$100.00</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="150">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="150">
                    <text fontName="标宋" fontSize="12">5:可口可乐一瓶</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="100">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
                    <text  fontName="仿宋" fontSize="12">$8.00</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="150">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="150">
                    <text fontName="标宋" fontSize="12">6:冰冻丸子</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="100">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
                    <text  fontName="仿宋" fontSize="12">$20.00</text>
                </para>
            </rect>
        </para>



        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="8">---------------------------------------------------------------------</text>
        </para>
        <para  lineSpacing="3" >
            <rect color="" name="x6870" width="150">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="150">
                    <text fontName="标宋" fontStyle="bold" fontSize="16">合计:</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="100">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
                    <text  fontName="仿宋" fontSize="12"> $393.50</text>
                </para>
            </rect>
        </para>
        </body>
    </xdoc>
</script>
</body>
</html>
